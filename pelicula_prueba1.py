#!/usr/bin/env python3
# -*- coding:utf-8¡ -*-


class Pelicula():
    def __init__(self):
        self.nombre = ""
        self.year = 0
        self.generos = []
        self.descripcion = None

    def get_nombre(self):
        return self.nombre

    def set_nombre(self, nombre):
        if isinstance(nombre, str):
            self.nombre = nombre

    def get_descripcion(self):
            return self.descripcion

    def set_descripcion(self, nombre):
                self.descripcion = nombre
