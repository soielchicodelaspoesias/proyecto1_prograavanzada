#!/usr/bin/env python3
# -*- coding:utf-8 -*-
import pandas as pd
import os
from pelicula import Pelicula



def obtener_datos():
    data = pd.read_csv("IMDb movies.csv")
    return data


def crea_pelicula(movies_2018):
    peliculas = []
    for i, nombre in movies_2018.iterrows():
        obj_pelicula = Pelicula()
        obj_pelicula.set_nombre(nombre["title"].capitalize())
        obj_pelicula.set_descripcion(nombre["description"])
        peliculas.append(obj_pelicula)
    return peliculas


def buscador(peliculas):
    a = input("Ingrese el nombre de la pelicula que desea buscar del 2018")
    for i in peliculas:
        if a == i.get_nombre():
            os.system("clear")
            print("{} \n {}".format("Nombre de la pelicula: "i.get_nombre(),
                                   "Descripcion: "i.get_descripcion()))

if __name__ == "__main__":
    data = obtener_datos()
    new = data.filter(["title", "genre", "year", "description"])
    movies_2018 = new[new["year"]==2018]
    peliculas = crea_pelicula(movies_2018)
    buscador(peliculas)
